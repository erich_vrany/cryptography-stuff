"""
Script to implement Pollard's attack on RSA.
Enter N, the RSA modulus, when prompted and the program will output p, q, and phi.
"""

import math

N = int(input("N: "))

c = 2
for i in range(1, 10001):
    c = pow(c, i, N)

    # Breaks the loop when it finds a gcd to avoid overshooting.
    if math.gcd(c - 1, N) != 1:
        break

c= (c - 1) % N
p = math.gcd(c, N)
q = N // p
phi = (p - 1) * (q - 1)

print(f"p = {p}")
print(f"q = {q}")
print(f"phi = {phi}")
print(f"pq = {p * q}")
