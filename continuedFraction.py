# Sources:
# Trappe and Washington - Cryptography with Coding Theory
# GeeksforGeeks | What is the maximum possible value of an integer in Python ? | https://www.geeksforgeeks.org/what-is-maximum-possible-value-of-an-integer-in-python/
# tutorialspoint | Python - Functions | https://www.tutorialspoint.com/python/python_functions.htm
# Goodger and Rossum | PEP 257 -- Docstring Conventions | https://www.python.org/dev/peps/pep-0257/
# Python Reference (The Right Way) | is_integer | https://python-reference.readthedocs.io/en/latest/docs/float/is_integer.html
# tutorialspoint | Python - Strings | https://www.tutorialspoint.com/python/python_strings.htm
# tutorialspoint | Python - Lists | https://www.tutorialspoint.com/python/python_lists.htm
# CrystalMath | Continued Fractions #3: Quickly Finding Continued Fractions of Rational Numbers | https://youtu.be/bOyNOT4lvTA
# GeeksforGeeks | How to print without newline in Python? | https://www.geeksforgeeks.org/print-without-newline-python/
# tutorialspoint | Fraction module in Python | https://www.tutorialspoint.com/fraction-module-in-python
# Python Documentation | fractions -- Rational numbers | https://docs.python.org/3.8/library/fractions.html
# Python Documentation | pdb -- The Python Debugger | https://docs.python.org/3/library/pdb.html
# Real Python | Python Debugging With Pdb | https://realpython.com/python-debugging-pdb/
# Bard, Gregory | private correspondence


import math
from typing import List
from fractions import Fraction

def getExpansion(e, N):
    """
    Return the continued fraction expansion of e / N as a List.
    e and N must be positive integers.
    """
    
    # base case
    if e % N == 0:
        return [math.floor(e / N)]

    return [math.floor(e / N)] + getExpansion(N, e % N)

def getValidInput(prompt):
    """ Return the user's input once it is a positive integer. """
    
    ret = ""

    while True:
        ret = input(prompt)

        if not ret.isdigit():
            print(f"{ret} is not a positive integer.")
            print("Please input a positive integer")
            continue
        
        ret = int(ret)
        return ret

def printBracketStyle(expansion: List):
    """
    Print the continued fraction expansion in bracket-style notation.
    expansion must be a List.
    """

    print(f"[ {expansion[0]};", end = " ")

    for i in range(1, len(expansion) - 1):
        print(f"{expansion[i]},", end = " ")

    print(f"{expansion[len(expansion) - 1]} ]")

def invertFraction(frac: Fraction):
    """
    Return the reciprocal of frac.
    Need to do it this way to avoid floating point errors.
    """
    numerator = frac.denominator
    denominator = frac.numerator
    return Fraction(numerator, denominator)

def getConvergent(expansion, end):
    """
    Return the convergents from expansion up to index end.
    """
    convergents = []
    for i in range(0, end + 1):
        convergents.append(Fraction(expansion[i]))

    i = end
    while i > 0:
        convergents[i] = invertFraction(convergents[i])
        convergents[i - 1] = convergents[i - 1] + convergents[i]
        
        i -= 1
        
    return convergents[0]

def squareRoot(num: int):
    """ Return the square root of num using the Babalonian method."""

    N = Fraction(num)
    
    # get in the ballpark
    length = math.floor(math.log2(num)) >> 1
    G = Fraction(2 ** length)

    for i in range(0, 1000):
        H = Fraction(math.floor(N * invertFraction(G)))
        g = (H.numerator + G.numerator) >> 1
        G = Fraction(g)

    return G.numerator

def getRoots(b: int, c: int, sign: int):
    """ 
    Return a root of the quadratic equation x^2 + bx + c. 
    If sign = -1, return the lower root.
    If sign = 1, return the upper root.
    """
    discriminant = b * b - 4 * c
    sqrtDiscriminant = squareRoot(discriminant)

    return -b + sign * (sqrtDiscriminant) >> 1

e = getValidInput("e: ")
N = getValidInput("N: ")
d = 0
C = 0

expansion = getExpansion(e, N)

printBracketStyle(expansion)
   
testConvergent = Fraction(-1)

for i in range(1, len(expansion), 2):
    testConvergent = getConvergent(expansion, i)
    print(testConvergent)

    if pow(pow(2, e, N), testConvergent.denominator, N) == 2:
        d = testConvergent.denominator
        print(f"Tested successfully. {d} is d.")
        break
    else:
        print("Tested unsuccessfully.")

C = Fraction(e * d - 1) * Fraction(1, testConvergent.numerator)
b = -N + C.numerator - 1
p = getRoots(b, N, -1)
q = getRoots(b, N, 1)
phi = (p - 1) * (q - 1)

print(f"p = {p}")
print(f"q = {q}")
print(f"N = {N}")
print(f"phi = {phi}")
print(f"e = {e}")
print(f"d = {d}")
