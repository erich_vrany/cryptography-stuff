#Sources consulted
#
#Trappe & Washington, Cryptography with Coding Theory

#Tutorialspoint:
#   Python 3 - Basic Operators https://www.tutorialspoint.com/python3/python_basic_operators.htm
#   Python - Command Line Arguments https://www.tutorialspoint.com/python/python_command_line_arguments.htm
#   Python - Strings https://www.tutorialspoint.com/python/python_strings.htm
#   Python - Lists https://www.tutorialspoint.com/python/python_lists.htm
#
#Python Numpy - GeeksforGeeks https://www.geeksforgeeks.org/python-numpy/
#ASCII Table https://www.asciitable.com/
#Statistics Globe - Strip Newline in Python | 4 Example Codes (Remove Trailing & Leading Blank Line) https://statisticsglobe.com/python-strip-remove-newline-from-string

#for reading the command line arguments
import sys

#for working with vectors
import numpy as np

#array containing English language text frequencies
ENGLISH_FREQUENCIES = np.array([0.082, 0.015, 0.028, 0.043, 0.127, 0.022, 0.20, 0.061, 0.070, 0.002, 0.008, 0.040, 0.024, 0.067, 0.075, 0.019, 0.001, 0.060, 0.063, 0.091, 0.028, 0.010, 0.023, 0.001, 0.020, 0.001])

#opens the file specified by the command line argument and stores the text
file = open(sys.argv[1],"r")
cyphertext = file.read()
file.close()

#for testing
#print(cyphertext)

#the test cyphertexts had new lines in them
#this gets rid of them
cyphertext = cyphertext.replace("\r\n", "")
cyphertext = cyphertext.replace("\n", "")

length = len(cyphertext)
coincidence = [0] * 21

for i in range(1, 21):
    for j in range(0, length):
        if (cyphertext[j] == cyphertext[(j + i) % length]):
                coincidence[i] += 1
    print(f"The index of coincidence for a displacement of {i} is {coincidence[i]}")

findCoin = np.array(coincidence)

keySuggestion = np.argmax(findCoin)

#keySuggestion = list.index(max(coincidence))

testLength = keySuggestion

while 2 < testLength // 2:
    testLength = testLength // 2
    if coincidence[testLength] < coincidence[keySuggestion] * 0.85:
        keySuggestion = keySuggestion // 2

print(f"Key length suggestion: {keySuggestion}")

keyLength = int(input("What key length would you like to choose? "))

bucket = [""] * keyLength

for i in range(0, length):
    bucket[i % keyLength] += cyphertext[i]
"""
for i in range(0, keyLength):
    print(f"Bucket {i}:")
    print(bucket[i])
"""
letterCount = [[0] * 26 for x in range(keyLength)]
dproducts = [0] * 26
keyNum = [0] * keyLength

for i in range(0, keyLength):
    for j in range(0, len(bucket[i])):
        num = ord(bucket[i][j]) - 65
        if (0 <= num & num < 26):
            letterCount[i][num] += 1

    letterFreq = np.array(letterCount[i])
    letterFreq = letterFreq / len(bucket[i])
    
    for j in range(0, 26):
        dproducts[j] = np.dot(letterFreq, np.roll(ENGLISH_FREQUENCIES, j))

    #print(dproducts)
    
    temp = np.argmax(dproducts)
    keyNum[i] = temp

key = ""

for i in keyNum:
    key += chr(i + 65)

print(f"The key is {key}")

cleartext = ""

cypherNum = [0] * length

for i in range(0, length):
    cypherNum = ord(cyphertext[i]) - 65
    cleartext += chr(((cypherNum - keyNum[i % keyLength]) % 26) + 97)

print(cleartext)

